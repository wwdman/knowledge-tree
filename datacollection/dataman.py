import json

def getUnitDetails(name):
    detail = name.split(" - ")

    if len(detail) == 1:
        return name.split(' ', 1)

    return detail


feeds = []
units = []

with open("data-test.json", mode='r') as f:
    feeds = json.load(f)
    f.close()



blankUnit = {
	"id": "", 
	"name": "", 
	"short_name": "",
	"discipline": "",
	"completed": False,
	"size": "",
	"children": []
}

'''
discip = []

#['PROGRAMMING', 'COMMUNICATION AND MEDIA STUDIES NEC', 'SYSTEMS ANALYSIS AND DESIGN', 'MATHEMATICS', 'NETWORKS AND COMMUNICATIONS', 'INFORMATION SYSTEMS NOT ELSEWHERE CLASSIFIED', 'COMPUTER SCIENCE NOT ELSEWHERE CLASSIFIED', 'MATHEMATICAL SCIENCES NOT ELSEWHERE CLASSIFIED', 'DATA STRUCTURES', 'INFORMATION TECHNOLOGY NOT ELSEWHERE CLASSIFIED', 'SECURITY SCIENCE', 'PHYSICS', 'STATISTICS']

for unit in feeds:
	if not unit["discipline"] in discip:
		discip.append(unit["discipline"])

print(len(discip))
print(len(feeds))
'''


for unit in feeds:

	newUnit = {
		"id": "", 
		"name": "", 
		"short_name": "",
		"discipline": "",
		"completed": False,
		"size": "",
		"children": [],
		"parents": []
	}

	newUnit["id"] = unit["unit_number"]
	newUnit["name"] = unit["unit_name"]
	newUnit["completed"] = True
	newUnit["discipline"] = unit["discipline"]
	newUnit["size"] = unit["unit_level"]

	print("\tUnit: %s" % (newUnit["name"]))

	if not unit["prerequisites"] == None:
		print("Prerequisit")

		for prereq in unit["prerequisites"]:

			if len(prereq) == 1:
				unitDetail = getUnitDetails(equiv)
				print(unitDetail)

				preqUnit = {
					"id": unitDetail[0],
					"name": unitDetail[1],
					"discipline": unit["discipline"],
					"size": unit["unit_level"],
					"completed": False
				}

				eqUnits["children"].append(preqUnit)

			else: 
				eqUnits = {
					"name": "EQ",
					"children": []
				}

				for equiv in prereq:
					unitDetail = getUnitDetails(equiv)
					print(unitDetail)

					preqUnit = {
						"id": unitDetail[0],
						"name": unitDetail[1],
						"discipline": unit["discipline"],
						"size": unit["unit_level"],
						"completed": False
					}

					eqUnits["children"].append(preqUnit)

				newUnit["parents"].append(eqUnits)
	
	if not unit["children"] == None:
		print("Equivilant")

		eqUnits = {
			"name": "EQ",
			"children": []
		}

		for equiv in unit["children"]:
			unitDetail = getUnitDetails(equiv)
			#print(unitDetail)

			eqUnit = {
				"id": unitDetail[0],
				"name": unitDetail[1],
				"discipline": unit["discipline"],
				"size": unit["unit_level"],
				"completed": False
			}

			eqUnits["children"].append(eqUnit)
			print(eqUnits)
			print(eqUnits["children"])

		eqUnits["children"].append(newUnit)
		
		# Add the new unit to the nodes array
		units.append(eqUnits)

	else:
		# Add the new unit to the nodes array
		units.append(newUnit)



## a regular unit can have an equivilant child OR a Prereq child


#print(units)

with open("data-testing2.json", mode='w+') as fp:
	fp.write(json.dumps(units, indent=4))


