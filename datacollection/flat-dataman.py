import json
import re

def getUnitDetails(name):
    detail = name.split(" - ")

    if len(detail) == 1:
        return name.split(' ', 1)

    return detail



def getShortName(name):
	newName = name
	shortn = ""
	bracket = re.search('.*(\(.*\)).*', newName, re.IGNORECASE)

	if bracket:
		newName = name.replace(bracket.group(1), '')

	for word in newName.split(): 
		shortn += word[0]

	if bracket:
		return shortn + bracket.group(1)

	else:
		return shortn




feeds = []
units = []
nodes = [
	{
	"id": 0,
	"name": "Daniel Stratti",
    "short_name": "DS (ME)",
    "color": "#03a9f4",
    "size": 2.95}
]
links = []
eqIndex = -1

with open("data-test.json", mode='r') as f:
    feeds = json.load(f)
    f.close()



blankUnit = {
	"id": "", 
	"name": "", 
	"short_name": "",
	"discipline": "",
	"completed": False,
	"size": "",
	"children": []
}

'''
discip = []

#['PROGRAMMING', 'COMMUNICATION AND MEDIA STUDIES NEC', 'SYSTEMS ANALYSIS AND DESIGN', 'MATHEMATICS', 'NETWORKS AND COMMUNICATIONS', 'INFORMATION SYSTEMS NOT ELSEWHERE CLASSIFIED', 'COMPUTER SCIENCE NOT ELSEWHERE CLASSIFIED', 'MATHEMATICAL SCIENCES NOT ELSEWHERE CLASSIFIED', 'DATA STRUCTURES', 'INFORMATION TECHNOLOGY NOT ELSEWHERE CLASSIFIED', 'SECURITY SCIENCE', 'PHYSICS', 'STATISTICS']

for unit in feeds:
	if not unit["discipline"] in discip:
		discip.append(unit["discipline"])

print(len(discip))
print(len(feeds))
'''


for unit in feeds:

	newUnit = {
		"id": "", 
		"name": "", 
		"short_name": "",
		"discipline": "",
		"completed": False,
		"size": ""
	}

	newUnit["id"] = unit["unit_number"]
	newUnit["name"] = unit["unit_name"]
	newUnit["completed"] = True
	newUnit["discipline"] = unit["discipline"]
	newUnit["size"] = unit["unit_level"]
	newUnit["short_name"] = getShortName(unit["unit_name"])


	print("\tUnit: %s" % (newUnit["name"]))

	'''
	# if the unit has equivilant units
	if not unit["children"] == None and len(unit["children"]) > 0:
		
		print("Equivilant")

		# Make a phantom node to join the equivilants
		eqUnits = {"id": eqIndex, "name": "EQ"}
		eqIndex -= 1

		# Add the phantom to the node list and grab its index as a source
		print("Adding Phantom Unit: %s" % (eqUnits["id"]))
		nodes.append(eqUnits)
		source = len(nodes) - 1


		# For each of the equivilant units add them to the node list with a link
		# to the phantom node.
		for equiv in unit["children"]:
			unitDetail = getUnitDetails(equiv)

			# make sure this unit is not already in the list
			target = next((index for (index, d) in enumerate(nodes) if d["id"] == unitDetail[0]), None)

			if target == None:
				# make a new unit
				eqUnit = {
					"id": unitDetail[0],
					"name": unitDetail[1],
					"short_name": getShortName(unitDetail[1]),
					"discipline": unit["discipline"],
					"size": unit["unit_level"],
					"completed": False
				}

				# add unit to list and create a link to the phantom node
				print("Adding Equivilant Unit: %s" % (eqUnit["id"]))
				nodes.append(eqUnit)
				links.append({"source": source, "target": (len(nodes) -1)})

			else:
				# add a node from the 
				links.append({"source": source, "target": target})

		# Add the new unit to the phantom node
		print("Adding Actual Unit: %s" % (newUnit["id"]))
		nodes.append(newUnit)
		links.append({"source": source, "target": (len(nodes) -1), "strength": 0.1})

		# set target fro prereqs
		targetMain = source


	else:
	'''
	
	# Add the new unit to the nodes array
	nodes.append(newUnit)

	# set target fro prereqs
	targetMain = (len(nodes) -1)


	# ----------------------- PREREQ -------------------------------------------
	# if the unit has prereqs
	if not unit["prerequisites"] == None and len(unit["prerequisites"]) > 0:
		print("Prerequisit")

		for prereq in unit["prerequisites"]:
			
			if len(prereq) == 1:
				unitDetail = getUnitDetails(prereq[0])
				# make sure this unit is not already in the list

				source = next((index for (index, d) in enumerate(nodes) if d["id"] == unitDetail[0]), None)

				if source == None:
					'''
					preqUnit = {
						"id": unitDetail[0],
						"name": unitDetail[1],
						"short_name": getShortName(unitDetail[1]),
						"discipline": unit["discipline"],
						"size": unit["unit_level"],
						"completed": False
					}

					# add unit to list and create a link to the phantom node
					print("Adding Prerequisit Unit: %s" % (preqUnit["id"]))
					nodes.append(preqUnit)
					links.append({"source": (len(nodes) -1), "target": targetMain})
					'''
				else:

					# Are there any links where the source index is the target & the source is < 0
					eqSource = next((d for (index, d) in enumerate(links) if (d["target"] == source and d["source"] < 0)), None)
					
					if eqSource == None:
						# add a node from the 
						links.append({"source": source, "target": targetMain})
					else:
						links.append({"source": eqSource["source"], "target": targetMain})


			else: 
				'''
				# Make a phantom node to join the equivilants
				eqUnits = {"id": eqIndex, "name": "EQ"}
				eqIndex -= 1

				# Add the phantom to the node list and grab its index as a source
				print("Adding Phantom Unit: %s" % (eqUnits["id"]))
				nodes.append(eqUnits)
				sourceMain = len(nodes) - 1
				'''

				for equiv in prereq:
					unitDetail = getUnitDetails(equiv)
					#print(unitDetail)

					source = next((index for (index, d) in enumerate(nodes) if d["id"] == unitDetail[0]), None)

					if source == None:
						'''
						preqUnit = {
							"id": unitDetail[0],
							"name": unitDetail[1],
							"short_name": getShortName(unitDetail[1]),
							"discipline": unit["discipline"],
							"size": unit["unit_level"],
							"completed": False
						}

						# add unit to list and create a link to the phantom node
						print("Adding Prerequisit Unit: %s" % (preqUnit["id"]))
						nodes.append(preqUnit)
						links.append({"source": (len(nodes) -1), "target": sourceMain})
						'''
					else:

						# Are there any links where the source index is the target & the source is < 0
						eqSource = next((d for (index, d) in enumerate(links) if (d["target"] == source and d["source"] < 0)), None)
						
						if eqSource == None:
							# add a node from the 
							links.append({"source": source, "target": targetMain})
						else:
							links.append({"source": eqSource["source"], "target": targetMain})

						'''
						# Are there any links where the source index is the target & the source is < 0
						eqSource = next((d for (index, d) in enumerate(links) if (d["target"] == source and d["source"] < 0)), None)
						
						if eqSource == None:
							# add a node from the 
							links.append({"source": source, "target": sourceMain})
						else:
							links.append({"source": eqSource["source"], "target": sourceMain})
						'''

				# add a node from the 
				#links.append({"source": sourceMain, "target": targetMain})

	else:
		# add a node from the 
		links.append({"source": 0, "target": targetMain})



## a regular unit can have an equivilant child OR a Prereq child


#print(units)

toPrint = {
	"nodes": nodes,
	"links": links
}

with open("data-testing8.json", mode='w+') as fp:
	fp.write(json.dumps(toPrint, indent=4))
