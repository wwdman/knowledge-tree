#!/usr/bin/python
# Scrape information from the WSU Handbook
# Author: Laurence Park, 2019


import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import json


def download_page(unit_number):
    # tell handbook that this is Chrome
    ua = UserAgent()
    header = {'User-Agent':str(ua.chrome)}
    
    url_base = "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit="
    # create URL
    url = url_base + unit_number
    # download page
    response = requests.get(url, headers=header)
    # parse page
    soup = BeautifulSoup(response.content, "html.parser")
    return(soup)

    
# cleaning functions
def check_empty_sibling(x):
    if (not x):
        return(None)
    else:
        if not x.next_sibling == None:
            return(x.next_sibling.next_sibling.replace('\n','').replace('\t','').replace('\r',' '))

def seperate_list(x, sep):
    if (not x):
        return(None)
    x_set = x.split(sep)
    x_set = [y.strip() for y in x_set]
    return(x_set)


def clean_unit_set(x):
    y = seperate_list(x, ';')
    if (y and (len(y) == 1)):
        return(seperate_list(y[0], ','))
    return(y)


def clean_prereqs(x):
    if (not x):
        return(None)
    y_set = seperate_list(x, 'AND')
    z = [seperate_list(y, 'OR') for y in y_set]

    req = []

    for y in y_set:
        req.append(seperate_list(y, 'OR'))
    return(req)


def extract_unit_information(soup):
    # extract information
    unit_information = {
        "school" : soup.find_all('span')[0].text,
        "unit_name" : soup.find_all('span')[1].text,
        "unit_number" : soup.find_all('span')[2].next_sibling.split('.')[0],
        "unit_version" : soup.find_all('span')[2].next_sibling.split('.')[1],
        "discipline" : soup.find_all('span')[3].next_sibling,
        "student_contribution_band" : soup.find_all('span')[4].next_sibling,
        "unit_level" : soup.find_all('span')[5].next_sibling,
        "cps" : soup.find_all('span')[6].next_sibling,
        "coordinator" : soup.find_all('a')[13].text,
        "prerequisites" : clean_prereqs(check_empty_sibling(soup.find('span', string = "Prerequisite"))),
        "enrolment_restrictions" : check_empty_sibling(soup.find('span', string = "Unit Enrolment Restrictions")),
        "equivalent_units" : clean_unit_set(check_empty_sibling(soup.find('span', string = "Equivalent Units"))),
        "incompatible_units" : clean_unit_set(check_empty_sibling(soup.find('span', string = "Incompatible Units"))),
        "assumed_knowledge" : check_empty_sibling(soup.find('span', string = "Assumed Knowledge"))
    }
    return(unit_information)

def safeAddNodesString(uid, name, children):
    global nodes

    # if this unit is not in the node list add it
    if not uid in nodes.keys():
        nodes[uid] = {"id": uid, "name": name, "children": children}

    # if it is in the list check if the pahntom is its child
    else:

        for child in children:
            if not child in nodes[uid]["children"]:
                nodes[uid]["children"].append(child)


def safeAddNodes(nodeToAdd):
    global nodes

    # if this unit is not in the node list add it
    if not nodeToAdd["id"] in nodes.keys():
        nodes[nodeToAdd["id"]] = nodeToAdd

    # if it is in the list check if the pahntom is its child
    else:

        for newChild in nodeToAdd["children"]:
            if not newChild in nodes[nodeToAdd["id"]]["children"]:
                nodes[nodeToAdd["id"]]["children"].append(newChild)

def getUnitDetails(name):
    detail = name.split(" - ")

    if len(detail) == 1:
        return name.split(' ', 1)

    return detail


completed_units = [
    "300580",
    "100483",
    "300585",
    "300700",
    "300565",
    "300581",
    "300104",
    "300582",
    "300144",
    "300095",
    "200025",
    "300103",
    "300570",
    "300698",
    "300167",
    "300583",
    "300958",
    "300143",
    "300128",
    "300578",
    "300579",
    "300828",
    "301108",
    "301033",
    "301110",
    "301204",
    "301109",
    "301111"
]

fname = "test2.json"

nodes = {}

nodeEmpty = {
    "id": 0,
    "name": "",
    "children": [],
    "level": 0
}
orNode = nodeEmpty
orNode["name"] = "OR node"

# "unit id": {"id": "unit id", "name": "", "children": []}
for unit in completed_units:
    # download parsed HTML
    soup = download_page(unit)
    # extract information
    unit_information = extract_unit_information(soup)

    currentNode = {
        "id": unit_information["unit_number"],
        "name": unit_information["unit_name"],
        "children": []
    }

    if not unit_information["equivalent_units"] == None:
        # Create Phantom Node
        phantom = orNode
        phId = []

        for equiv in unit_information["equivalent_units"]:

            # Get the unit ID and Name and add the phantom to the equivilant
            unitDetail = getUnitDetails(equiv)



            print(unitDetail)


            uid = unitDetail[0]
            name = unitDetail[1]

            phId.append(uid)
            eqNode = nodeEmpty
            eqNode["id"] = uid
            eqNode["name"] = name

            phantom["children"].append(eqNode)

            #safeAddNodesString(uid, name, [phantom])

        # Add the current node to the phantoms 
        phantom["id"] = "-".join(phId)
        phantom["children"].append(currentNode)

        safeAddNodes(phantom)

'''
    if not unit_information["prerequisites"] == None:
        for prereq in unit_information["prerequisites"]:
            # if prereq length is one just add the current node to its child
            if len(prereq) == 1:
                for equiv in prereq[0]:

                if not currentNode in prereq[0]["children"]:
                    prereq[0]["children"].append(currentNode)

            else:
                # Create Phantom Node
                phantom = orNode
                phId = []

                # Loop over the equivilant prereqs
                for equiv in prereq:

                    # Get the unit ID and Name and add the phantom to the equivilant
                    uid, name = equiv.split(" - ")
                    phId.append(uid)

                    safeAddNodesString(uid, name, [phantom])

                # Add the current node to the phantoms 
                phantom["children"].append(currentNode)
                phantom["id"] = "-".join(phId)
                safeAddNodes(phantom)
'''

    


'''

                # if this unit is not in the node list add it
                if not uid in nodes.keys():
                    nodes[uid] = {"id": uid, "name": name, "children": [phantom]}

                # if it is in the list check if the pahntom is its child
                elif not phantom in nodes[uid]["children"]:
                    nodes[uid]["children"].append(phantom)
                    
                # nodes[uid].children.append(node)

        
        





            uid, name = pre.split(" - ")

            if not uid in nodes.keys():
                nodes[uid] = {"id": uid, "name": name, "children": []}
                
            nodes[uid].children.append(node)
'''


'''
store unit in nodes as a child of any of it prerequisits
    create a phantom node as the child of any OR relationships

create a phantom node as the child of any equivilant units
'''

with open(fname, mode='w') as f:
    f.write("[]")

#parsed = json.loads(unit_information)
print(json.dumps(nodes, indent=4, sort_keys=True))

with open(fname) as feedsjson:
    feeds = json.load(feedsjson)

nodeList = []

for key in nodes.keys():
    nodeList.append(nodes[key])

feeds.append(nodeList)
with open(fname, mode='w') as f:
    f.write(json.dumps(feeds, indent=4))
#print(unit_information)


'''
LOGIC FOR TREE

A root node (unit) is a unit that has no prerequisites


A node can be related to units as a:
    - prerequisite
    - equivilant

Prerequisite units come in two types of relationships, the fist compounding (AND)
the second OR

AND relationships lead to direct node connection, OR relationships have a 
phantom node between the ORs leading to this NODE

Equivilant units are related by a common phantomn node to represent the OR 
relationship. 

'''




